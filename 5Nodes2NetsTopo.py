from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import Node, OVSSwitch, RemoteController
from mininet.log import setLogLevel, info
from mininet.cli import CLI

class LinuxRouter( Node ):
    "A Node with IP forwarding enabled."

    def config( self, **params ):
        super( LinuxRouter, self).config( **params )
        # Enable forwarding on the router
        self.cmd( 'sysctl net.ipv4.ip_forward=1' )

    def terminate( self ):
        self.cmd( 'sysctl net.ipv4.ip_forward=0' )
        super( LinuxRouter, self ).terminate()

class NetworkTopo( Topo ):
    "A LinuxRouter connecting three IP subnets"

    def build( self, **_opts ):
    
        ### Declaring Routers ###
        r0 = self.addNode( 'r0', cls=LinuxRouter, ip='192.168.1.1/24', 
                defaultRoute='via 192.168.2.2' )
        r1 = self.addNode( 'r1', cls=LinuxRouter, ip='192.168.2.2/24',
                defaultRoute='via 192.168.2.1')
        ### Declaring switches ###
        s0, s1, s2= [ self.addSwitch( s ) for s in ( 's0', 's1', 's2' ) ]
        ### Linking Routers with switches ###
        self.addLink( s0, r0, intfName2='r0-eth1',
                      params2={ 'ip' : '192.168.1.1/24' } )  # for clarity
        self.addLink( s1, r0, intfName2='r0-eth2',
                      params2={ 'ip' : '192.168.2.1/24' } )
        self.addLink( s1, r1, intfName2='r1-eth1',
                      params2={ 'ip' : '192.168.2.2/24' } )
        self.addLink( s2, r1, intfName2='r1-eth2',
                      params2={ 'ip' : '192.168.3.1/24' } )

        ### Declaring hosts ###
        h0 = self.addHost( 'h0', ip='192.168.1.100/24',
                           defaultRoute='via 192.168.1.1' )
        h1 = self.addHost( 'h1', ip='192.168.1.101/24',
                           defaultRoute='via 192.168.1.1' )
        h2 = self.addHost( 'h2', ip='192.168.2.100/24',
                           defaultRoute='via 192.168.2.1',
                           staticRoute='192.168.3.0/24, 192.168.2.2')
        h3 = self.addHost( 'h3', ip='192.168.2.101/24',
                           defaultRoute='via 192.168.2.1',
                           staticRoute='192.168.3.0/24, 192.168.2.2')
        h4 = self.addHost( 'h4', ip='192.168.3.100/24',
                           defaultRoute='via 192.168.3.1' )
        h5 = self.addHost( 'h5', ip='192.168.3.101/24',
                           defaultRoute='via 192.168.3.1' )

        ### Linking hosts with switches ###
        for h, s in [ (h0, s0), (h1, s0), (h2, s1), (h3, s1), (h4, s2), (h5, s2) ]:
            self.addLink( h, s )

def run():
    "Test linux router"
    topo = NetworkTopo()
    net = Mininet( topo=topo, 
            controller=lambda name: RemoteController( name, ip='127.0.0.1', port=6633 ), 
            switch=OVSSwitch )
    net.start()
    info( '*** Routing Table on Router:\n' )
    info( net[ 'r0' ].cmd( 'route' ) )
    info( net[ 'r1' ].cmd( 'route' ) )
    CLI( net )
    #net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    run()
